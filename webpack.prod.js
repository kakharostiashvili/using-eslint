const { merge } = require('webpack-merge'); // eslint-disable-line
const common = require('./webpack.config');

module.exports = merge(common, {
  mode: 'production',
});
