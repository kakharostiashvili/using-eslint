module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    'airbnb-base',
  ],
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  rules: {
    'no-dupe-args': 'error',
    'no-func-assign': 'error',
    'no-dupe-else-if': 'error',
    'for-direction': 'error',
    'no-duplicate-case': 'error',
  },
};
